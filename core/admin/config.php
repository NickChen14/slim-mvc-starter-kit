<?php

return [
    'config' => [
           'displayErrorDetails' => true,
             'defaultController' => 'home',
                      'viewPath' => APPSPATH . 'View'
    ],
    'settings' => [
        // Only set this if you need access to route within middleware
        'determineRouteBeforeAppMiddleware' => true
    ]
];

