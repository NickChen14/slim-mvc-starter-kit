<?php

$container = $app->getContainer();

// Log
$container['log'] = function ($c) {
    return new AdminApp\Librarie\Log();
};

// View
$container['view'] = function ($c) {
    $view = new League\Plates\Engine($c->get('config')['viewPath'], 'php');
    $view->loadExtension(new League\Plates\Extension\URI($_SERVER['PATH_INFO']));
    $view->addFolder('Layout', $c->get('config')['viewPath'] . DIRECTORY_SEPARATOR . 'layout');
    return $view;
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write($c->view->render('404'));
    };
};
