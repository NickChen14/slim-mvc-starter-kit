<?php

// Middleware(中介層)

$checkProxyHeaders = true;
$trustedProxies = ['10.0.0.1', '10.0.0.2'];
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));

$SignMiddleware = function ($request, $response, $next) {
    session_start();
    $route = $request->getAttribute('route');
    if($route == NULL) {
        // return $response->withRedirect('/admin/signin/');
        return $next($request, $response);
    }

    $name = $route->getName();
    $groups = $route->getGroups();
    $methods = $route->getMethods();
    $arguments = $route->getArguments();

    // $response->getBody()->write(var_dump($route));
    // exit;

    // # Define routes that user does not have to be logged in with. All other routes, the user
    // # needs to be logged in with.
    $publicRoutesArray = array(
        'signin',
        'signin/post',
        'signout',
        // 'forgot-password',
        // 'register-post'
    );

    if (!isset($_SESSION['USER']) && !in_array($name, $publicRoutesArray))
    {
        // redirect the user to the login page and do not proceed.
        $response = $response->withRedirect('/admin/signin/');

    }
    else
    {
        // Proceed as normal...
        $response = $next($request, $response);
    }

    return $response;
};

// Apply the middleware to every request.
$app->add($SignMiddleware);
