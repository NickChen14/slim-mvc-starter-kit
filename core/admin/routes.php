<?php

// 預設路徑
$default = 'AdminApp\\Controller\\' . ucwords($container->get('config')['defaultController']) . ':index';

$app->get('/', $default)->setName('default');

// 自定路徑
// $app->get('/{locale:en}/index', 'App\Controller\Home:index')->setName('home');
// $app->get('/{locale:zh}/index', 'App\Controller\Home:index')->setName('home');
// $app->get('/product/{id:[0-9]+}', 'App\Controller\Product:index')->setName('product');
// $app->response->redirect($app->urlFor('default'), 401);


$app->get('/signin/', 'AdminApp\Controller\Sign:index')->setName('signin');
$app->post('/signin/', 'AdminApp\Controller\Sign:post')->setName('signin/post');
$app->get('/signout/', 'AdminApp\Controller\Sign:signout')->setName('signout');

$app->get('/home/', 'AdminApp\Controller\Home:index')->setName('home');

$app->get('/sample/', 'AdminApp\Controller\Sample:list')->setName('sample/list');
$app->get('/sample/create/', 'AdminApp\Controller\Sample:create')->setName('sample/create');
$app->post('/sample/create/', 'AdminApp\Controller\Sample:create')->setName('sample/create/post');
$app->get('/sample/sort/{id:[0-9]+}', 'AdminApp\Controller\Sample:sort')->setName('sample/sort');
$app->get('/sample/delete/{id:[0-9]+}', 'AdminApp\Controller\Sample:delete')->setName('sample/delete');
$app->get('/sample/update/{id:[0-9]+}', 'AdminApp\Controller\Sample:update')->setName('sample/update');
$app->post('/sample/update/{id:[0-9]+}', 'AdminApp\Controller\Sample:update')->setName('sample/update/post');

$app->get('/account/', 'AdminApp\Controller\Account:index')->setName('account/index');
$app->post('/account/', 'AdminApp\Controller\Account:post')->setName('account/post');
