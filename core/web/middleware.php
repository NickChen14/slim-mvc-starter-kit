<?php

// Middleware(中介層)

$checkProxyHeaders = true;
$trustedProxies = ['10.0.0.1', '10.0.0.2'];
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));

$localeMiddleware = function ($request, $response, $next) {
    $route = $request->getAttribute('route');
    if($route == NULL) {
        return $next($request, $response);
    }
    $name = $route->getName();
    $groups = $route->getGroups();
    $methods = $route->getMethods();
    $arguments = $route->getArguments();

    // print "Route Info: " . print_r($route, true);
    // print "Route Name: " . print_r($name, true);
    // print "Route Groups: <code>" . print_r($request->getUri(), true) . "</code>";
    // print "Route Methods: " . print_r($methods, true);
    // print "Route Arguments: " . print_r($arguments, true);

    // $response->getBody()->write($request->getUri());
    // $uri = $request->getUri();
    // $path = explode('/', $uri->getPath());
    // $query = $uri->getQuery();
    // $query = $request->getQueryParams($query);
    
    // var_dump($uri->getPath());
    // $response->getBody()->write(print_r($ppp));
    // $response->getBody()->write('</pre>');
    // # Define routes that user does not have to be logged in with. All other routes, the user
    // # needs to be logged in with.
    $response = $next($request, $response);
    

    return $response;
};

// Apply the middleware to every request.
$app->add($localeMiddleware);
