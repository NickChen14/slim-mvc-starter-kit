<?php
use AdminApp\Model\TourPackage as ModelTourPackage;
// 預設路徑
$default = 'App\\Controller\\' . ucwords($container->get('config')['defaultController']) . ':index';

$app->get('/', $default)->setName('default');

// 自定路徑
// $app->get('/{locale:en}/index', 'App\Controller\Home:index')->setName('home');
// $app->get('/{locale:zh}/index', 'App\Controller\Home:index')->setName('home');
// $app->get('/product/{id:[0-9]+}', 'App\Controller\Product:index')->setName('product');
// $app->response->redirect($app->urlFor('default'), 401);


$app->group('/zh', function () {
    $this->get('/home', 'App\Controller\Home:index')->setName('home');
    $this->get('/sample/', 'App\Controller\Sample:list')->setName('sample/list');
    $this->get('/sample/{id:[0-9]+}', 'App\Controller\Sample:detail')->setName('sample/detail');
});

$app->group('/en', function () {
    $this->get('/home', 'App\Controller\Home:index')->setName('home');
    $this->get('/sample/', 'App\Controller\Sample:list')->setName('sample/list');
    $this->get('/sample/{id:[0-9]+}', 'App\Controller\Sample:detail')->setName('sample/detail');
});

$app->post('/sample-api/', 'App\Controller\SampleApi:index')->setName('sample/index');
