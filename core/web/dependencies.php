<?php

$container = $app->getContainer();

// Log
$container['log'] = function ($c) {
    return new AdminApp\Librarie\Log();
};

// View
$container['view'] = function ($c) {
    $view = new League\Plates\Engine($c->get('config')['viewPath'], 'php');
    $view->loadExtension(new League\Plates\Extension\URI($c->request->getUri()->getPath()));
    $view->addFolder('Layout', $c->get('config')['viewPath'] . DIRECTORY_SEPARATOR . 'layout');
    // Register a one-off function
    $view->registerFunction('routeStr', function ($string) {
        $patterns = '/[\s\/\\:#=+<>]/';
        $replacements = '-';
        return strtolower(preg_replace($patterns, $replacements, $string));
    });

    // i18n
    $path = explode('/', $c->request->getUri()->getPath());
    $LibrarieLocale = new AdminApp\Librarie\Locale();
    $locale = $path[1];
    if( array_key_exists($locale, $LibrarieLocale::$LocaleRouter) ) {
        $localeName = $LibrarieLocale::$LocaleRouter[$locale];
        $localeHtml = $LibrarieLocale::$LocaleHtml[$locale];
        $LibrarieLanguage = 'AdminApp\\Librarie\\Language\\' . strtoupper($locale);
        $Language = new $LibrarieLanguage();
    }
    else {
        $localeName = $LibrarieLocale::$LocaleRouter['en'];
        $localeHtml = $LibrarieLocale::$LocaleHtml['en'];
        $Language = new AdminApp\Librarie\Language\EN();
    }

    // url
    $baseUrl = $c->request->getUri()->getBaseUrl();
    $baseLocaleUrl = $baseUrl . '/' . $locale . '/';

    // var_dump($breadcrumb);
    // var_dump($c->request->getUri()->getScheme());
    // var_dump($c->request->getUri()->getAuthority());
    // var_dump($c->request->getUri()->getUserInfo());
    // var_dump($c->request->getUri()->getHost());
    // var_dump($c->request->getUri()->getPort());
    // var_dump($c->request->getUri()->getPath());
    // var_dump($c->request->getUri()->getBasePath());
    // var_dump($c->request->getUri()->getQuery());
    // var_dump($c->request->getUri()->getFragment());
    // var_dump($c->request->getUri()->getBaseUrl());

    // Pass data to view
    // Utility
    $view->addData( ['locale'           => [ 'key' => $locale, 'name' => $localeName, 'html' => $localeHtml, 'all' => $LibrarieLocale::$LocaleRouter, 'text' => $Language->text ] ] );
    $view->addData( ['baseUrl'          => $baseUrl . '/' ] );
    $view->addData( ['baseLocaleUrl'    => $baseLocaleUrl ] );
    $view->addData( ['breadcrumb'       =>  $breadcrumb] );

    // Copy
    $view->addData( ['title'            => 'Sample'] );
    $view->addData( ['keywords'         => 'Slim,MVC,前後台管理系統'] );
    $view->addData( ['description'      => 'Slim-Skeleton-MVC + Medoo + League/Plates 製作前後台管理系統'] );
    $view->addData( ['image'            => $baseUrl . '/images/fb.png'] );
    

    
    

    return $view;
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write($c->view->render('404'));
    };
};
