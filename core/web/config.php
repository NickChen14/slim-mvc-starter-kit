<?php

return [
    'config' => [
           'displayErrorDetails' => true,
             'defaultController' => 'root',
                      'viewPath' => APPSPATH . 'View'
    ],
    'settings' => [
        // Only set this if you need access to route within middleware
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ]
];

