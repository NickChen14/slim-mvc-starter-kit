<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en" class="no-js"><!--<![endif]-->
    <head>
        <?php $this->insert('partials/head') ?>
    </head>
    <body>
        
        <div class="page">
            <div class="page-main">
                <?php $this->insert('partials/header') ?>    
                <div class="my-3 my-md-5">
                    <div class="container">
                        <?php echo $this->section('content'); ?>
                    </div>
                </div>
            </div>
            <?php $this->insert('partials/footer') ?>
        </div>

    
        
            
        
        
    
        
    </body>
</html>
