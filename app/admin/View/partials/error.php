<?php if(isset($error)): ?>
<div class="form-group">
    <div class="alert alert-danger" role="alert">
        <ul class="list-unstyled mb-0">
            <?php foreach ($error as $c): ?>
            <li><?=$this->e($c)?></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>
