<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/admin/">
                Management system
            </a>
            <div class="d-flex order-lg-2 ml-auto">
                
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>
</div>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 ml-auto">
                
            </div>
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="/admin/" class="nav-link"><i class="fe fe-home"></i> Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-file-text"></i> Sample</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="/admin/sample/" class="dropdown-item ">Lists</a>
                            <a href="/admin/sample/create/" class="dropdown-item ">Add</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/account/" class="nav-link"><i class="fe fe-user"></i> Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/signout/" class="nav-link"><i class="fe fe-log-out"></i> Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
