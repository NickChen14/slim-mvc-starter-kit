<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<title>Sample</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
<script src="/admin/assets/js/require.min.js"></script>
<script>
requirejs.config({
    baseUrl: '/admin'
});
</script>
<!-- Dashboard Core -->
<link href="/admin/assets/css/dashboard.css" rel="stylesheet" />
<script src="/admin/assets/js/dashboard.js"></script>
<!-- c3.js Charts Plugin -->
<link href="/admin/assets/plugins/charts-c3/plugin.css" rel="stylesheet" />
<script src="/admin/assets/plugins/charts-c3/plugin.js"></script>
<!-- Google Maps Plugin -->
<link href="/admin/assets/plugins/maps-google/plugin.css" rel="stylesheet" />
<script src="/admin/assets/plugins/maps-google/plugin.js"></script>
<!-- Input Mask Plugin -->
<script src="/admin/assets/plugins/input-mask/plugin.js"></script>
