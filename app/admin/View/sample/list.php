<?php $this->layout('Layout::default', ['data' => $data]); ?>

<div class="page-header">
    <h1 class="page-title">
        Sample
    </h1>
</div>
<div class="row row-cards row-deck">
    <div class="col-12">
        <div class="card">
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Avatar</th>
                            <th>Name</th>
                            <th>Order</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data['data'] as $k => $array):?>
                        <tr>
                            <td><span class="text-muted"><?=$array['id']?></span></td>
                            <td>
                                <span class="avatar" style="background-image: url(<?=$array['avatar']?>)"></span>
                            </td>
                            <td><?=$this->e($array['name'])?></td>
                            <td>
                                <a class="icon" href="/admin/sample/sort/<?=$array['id']?>?page=<?=$data['currentPage']?>&type=up" title="<?=$array['order_num']?>">
                                    <i class="fe fe-arrow-up"></i>
                                </a>
                                <a class="icon" href="/admin/sample/sort/<?=$array['id']?>?page=<?=$data['currentPage']?>&type=down">
                                    <i class="fe fe-arrow-down"></i>
                                </a>
                            </td>
                            <td>
                                <a class="icon" href="/admin/sample/update/<?=$array['id']?>">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a class="icon" href="/admin/sample/delete/<?=$array['id']?>?page=<?=$data['currentPage']?>">
                                    <i class="fe fe-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <nav class="">
            <ul class="pagination justify-content-center">
                <li class="page-item">
                <a class="page-link" href="?page=<?=(($data['currentPage']-1) < 1) ? '1' : $data['currentPage']-1; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
                </li>
                <li class="page-item">
                    <div class="page-link p-0">
                        <select class="form-control border-0" name="page" style="height: 34px;">
                            <?php for( $i = 1; $i <= $data['totalPages'] ; $i++): ?>
                            <option value="<?=$this->e($i)?>" <?=($i == $data['currentPage']) ? 'selected': ''?>><?=$this->e($i)?></option>
                            <?php endfor;?>
                        </select>
                    </div>
                </li>
                <li class="page-item">
                <a class="page-link" href="?page=<?=(($data['currentPage']+1) > $data['totalPages']) ? $data['totalPages'] : $data['currentPage']+1 ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<script>
    requirejs([
        'jquery',
    ], function($){
        $(function(){
            $('select[name=page]').change(function(){
                location.href = '?page=' + $(this).val();
            });
        });
    });
    
</script>
