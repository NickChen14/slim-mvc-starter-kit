<?php $this->layout('Layout::default', ['data' => $data]); ?>
<div class="page-header">
    <h1 class="page-title">
    Tour Packages
    </h1>
</div>
<div class="row row-cards row-deck">
    <div class="col-12">
        <form class="card" action="" method="post" enctype="multipart/form-data" onsubmit="return v();">
            <div class="card-body">
                <?php $this->insert('partials/error', [ 'error' => $error ]) ?>
                <div class="form-group">
                    <label class="form-label">Name<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="name" placeholder="Text.." value="<?=$data['data']['name']?>">
                    <div class="invalid-feedback">Required</div>
                </div>
                <div class="form-group">
                    <label class="form-label">TEL<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="tel" placeholder="Text.." value="<?=$data['data']['tel']?>">
                    <div class="invalid-feedback">Required</div>
                </div>
                <div class="form-group">
                    <div class="form-label">List Picture<span class="form-required">*</span> 
                        <?php if(isset($data['data']['id'])):?>
                        <a class="file-avatar" target="_blank" href="/uploads/sample/<?=$this->e($data['data']['id'])?>.<?=$this->e($data['data']['avatar'])?>"><i class="fe fe-image"></i></a>
                        <?php endif;?>
                        <span class="form-label-small">(Limited JPG/PNG 1M )</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="avatar">
                        <div class="invalid-feedback">Required</div>
                        <label class="custom-file-label">Choose file</label>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="/admin/assets/css/plugin/jquery-bootstrap-datepicker.css" rel="stylesheet">
<script>
    function v() {
        // return false;
        $('input[name^=name], input[name^=tel]').each(function(){
            if($(this).val() == '') {
                $(this).addClass('is-invalid');
            }
        });
        
        if($('.file-avatar').length == 0 && $('input[name=avatar]').val() == '') {
            $('input[name=avatar]').addClass('is-invalid');
        }

        if($('.is-invalid').is(':visible')) {
            return false;
        }
    }
    requirejs([
        'jquery',
    ], function($){
        $(function(){
            $('input[type=file]').on('change', function(){
                if($(this).val() == '') {
                    $(this).next('.custom-file-label').text('Choose file');
                }
                else {
                    var file = $(this).val().split('\\');
                    $(this).next('.custom-file-label').text(file[file.length-1]);
                }
            });
        });
    });
    
</script>
