<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <?php $this->insert('partials/head') ?>
</head>

<body>




    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                        <form class="card" action="/admin/signin/" method="post" onsubmit="return v()">
                            <div class="card-body p-5">
                                <div class="card-title">Login to your account</div>
                                <?php $this->insert('partials/error', [ 'error' => $error ]) ?>
                                <div class="form-group">
                                    <label class="form-label">Account</label>
                                    <input name="account" type="text" class="form-control" placeholder="Enter account" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">
                                    Password
                                    <!-- <a href="./forgot-password.html" class="float-right small">I forgot password</a> -->
                                    </label>
                                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">reCAPTCHA</label>
                                    <div class="g-recaptcha" data-sitekey="6LdbB2IUAAAAANGxQ6ngxvmykHRH-jp4RM4uWcba"></div>
                                    <div class="invalid-feedback">reCAPTCHA is required</div>
                                </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        function v() {
            var $captcha = $( '#recaptcha' ),
                response = grecaptcha.getResponse();
            
            if (response.length === 0) {
                if( !$captcha.hasClass( "error" ) ){
                    $('.g-recaptcha + .invalid-feedback').show();
                    return false;
                }
            } else {
                $('.g-recaptcha + .invalid-feedback').hide();
                return true;
                
            }
        }
    </script>
</body>

</html>
