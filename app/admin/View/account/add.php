<?php $this->layout('Layout::default', ['data' => $data]); ?>

<div class="page-header">
    <h1 class="page-title">
        Account
    </h1>
</div>
<div class="row row-cards row-deck">
    <div class="col-12">
        <form class="card" action="/admin/account/" method="post" onsubmit="return v();">
            <div class="card-body">
                <?php $this->insert('partials/error', [ 'error' => $error ]) ?>
                <div class="form-group">
                    <label class="form-label">Old Account<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="o_account" placeholder="Text.." readonly value="<?=$this->e($data['data']['o_account'])?>">
                </div>
                <div class="form-group">
                    <label class="form-label">Old Password<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="o_password" placeholder="Text.." readonly value="<?=$this->e($data['data']['o_password'])?>">
                </div>
                <div class="form-group">
                    <label class="form-label">New Account<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="account" placeholder="Text..">
                    <div class="invalid-feedback">Required</div>
                </div>
                <div class="form-group">
                    <label class="form-label">New Password<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="password" placeholder="Text..">
                    <div class="invalid-feedback">Required</div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <button type="reset" class="btn btn-secondary">Reset</button>
                    <button type="submit" class="btn btn-primary ml-auto">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function v() {
        if($('input[name=account]').val() == '') {
            $('input[name=account]').addClass('is-invalid');
        }

        if($('input[name=password]').val() == '') {
            $('input[name=password]').addClass('is-invalid');
        }

        if($('.is-invalid').is(':visible')) {
            return false;
        }
    }
</script>
