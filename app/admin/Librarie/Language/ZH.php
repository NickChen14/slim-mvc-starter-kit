<?php

namespace AdminApp\Librarie\Language;

/** 
* 語系
*  
* @category Model
* @package  Locale
**/
class ZH
{
    
    /**
     * 建構子
     */
    public function __construct()
    {
        $this->text['submit']                                       = "送出";
        $this->text['search']                                       = "搜尋";
        $this->text['all']                                          = "全部";
        $this->text['backtolist']                                   = "返回列表";
        $this->text['comment']                                      = "評論";
        $this->text['more-detail']                                  = "更多訊息";
        $this->text['contact']                                      = "聯絡我們";
    }
    
}
