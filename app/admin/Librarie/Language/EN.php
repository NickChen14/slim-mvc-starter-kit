<?php

namespace AdminApp\Librarie\Language;

/** 
* 語系
*  
* @category Model
* @package  Locale
**/
class EN
{
    
    /**
     * 建構子
     */
    public function __construct()
    {
        $this->text['submit']                                       = "Submit";
        $this->text['search']                                       = "Search";
        $this->text['all']                                          = "All";
        $this->text['backtolist']                                   = "Back to List";
        $this->text['comment']                                      = "Comment";
        $this->text['more-detail']                                  = "for more details";
        $this->text['contact']                                      = "contact us";
        
    }
    
}
