<?php

namespace AdminApp\Librarie;

/** 
* 語系
*  
* @category Model
* @package  Locale
**/
class Locale
{
    const ZH = '2';
    const EN = '1';
    

    static $Langs = array(
        self::EN => 'ENGLISH',
        self::ZH => '繁體中文'
    );
    static $LocaleRouter = array(
        'en' => 'ENGLISH',
        'zh' => '繁體中文'
    );
    static $LocalePath = array(
        self::EN => 'en',
        self::ZH => 'zh'
    );
    static $LocaleHtml = array(
        'en' => 'en',
        'zh' => 'zh-Hant-TW'
    );
    
}

