<?php

namespace AdminApp\Model;
use AdminApp\Librarie\Locale as LibrarieLocale;
use Slim\Http\UploadedFile;

/** 
* Sample
*  
* @category Model
* @package  Sample
**/
class Sample extends Base
{
    /**
     * 
     */
    const Path = ROOTPATH . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'sample';
    const Url  = DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'sample';
    /**
     * 
     */
    const Table = 'tbl_sample';
    /**
     * Undocumented function
     *
     * @param integer $page
     * @return void
     */
    public function list($page = 1) {
        // var_dump($id);
        
        $pageSize = 10;
        $page = (intval($page) <= 0) ? 1 : $page;
        $pageStart = ($page-1) * $pageSize;
        
        $sqlFrom = " FROM " . self::Table;
        $sqlWhere = "";
        
        $sql = "SELECT * " . $sqlFrom . $sqlWhere . "
            ORDER BY order_num ASC LIMIT " . $pageStart . " , " . $pageSize;
        $data = $this->db->query($sql)->fetchAll();

        foreach($data as $k => $v) {
            $data[$k]['avatar'] = self::Url . '/' . $v['id'] . '.' . $v['avatar'];
        }

        $sql = "SELECT count(*) as Count " . $sqlFrom . $sqlWhere;
        $dataCount = $this->db->query($sql)->fetchAll();
        $totalCount = $dataCount[0]['Count'];
        $totalPages = ceil( intval($totalCount) / $pageSize );
        $currentPage = $page;
        // var_dump( $this->db->log() );
        // var_dump( $this->db->error() );
       
        return [ 
            'data' => $data,
            'currentPage' => $page,
            'totalCount' => $totalCount,
            'totalPages' => $totalPages,
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $data
     * @return void
     */
    public function create($data, $uploadedFile) {    
        $id = 0;
        $this->db->action( function($db) use ($data, &$id, $uploadedFile) {
            
            // $this->log->write($data);
            
            // $order_num = $db->max(self::Table, 'order_num', [
            //     'category' => $data['category']
            // ]);
            // $order_num = intval($order_num) + 1;
            $ext = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);

            $db->update(
                self::Table, 
                [
                    "order_num[+]" => 1
                ]
            );

            $res = $db->insert(self::Table, [
                'name' => $data['name'],
                'tel' => $data['tel'],
                'avatar' => $ext,
                'order_num' => 1,
                'update_datetime' => date('Y-m-d H:i:s'),
                'create_datetime' => date('Y-m-d H:i:s')
            ]);

            $id = $db->id();

            $filename = $this->moveUploadedFile(self::Path, $uploadedFile, $id);
            // $response->write('uploaded ' . $filename . '<br/>');
            
            $error = $this->db->error();
            if( $error[2] != NULL ) {
                $this->log->write(json_encode($error));
                return false;
            }
            
        });


        
        // var_dump( $this->db->error() );

        return $id;
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function sort($id, $sortType) {

        
        $data = $this->db->get(
            self::Table , 
            [
                "id",
                "order_num",
            ], 
            [
                "id" => $id
            ]
        );

        $order_num = intval($data['order_num']);
        // var_dump($order_num);
        if($sortType == 'up') {
            if($order_num<=1) {
                return false;
            }
            
            $this->db->update(
                self::Table, 
                [
                    "order_num[+]" => 1
                ], 
                [
                    "order_num" => $data['order_num'] - 1,
                ]
            );

            $this->db->update(
                self::Table, 
                [
                    "order_num[-]" => 1
                ], 
                [
                    "id" => $id
                ]
            );


        }
        else if($sortType == 'down') {
            $max_num = $this->db->max(
                self::Table, 'order_num'
            );
            if($order_num >= $max_num) {
                return false;
            }

            $this->db->update(
                self::Table, 
                [
                    "order_num[-]" => 1
                ], 
                [
                    "order_num" => $data['order_num'] + 1,
                ]
            );

            $this->db->update(
                self::Table, 
                [
                    "order_num[+]" => 1
                ], 
                [
                    "id" => $id
                ]
            );

        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id) {
        $data = $this->db->get(
            self::Table , 
            [
                "id",
                "order_num",
                'avatar'
            ], 
            [
                "id" => $id
            ]
        );

        $this->db->delete(
            self::Table,
            [
                "id" => $id
            ]

        );

        $order_num = intval($data['order_num']);

        $this->db->update(
            self::Table, 
            [
                "order_num[-]" => 1
            ], 
            [
                "order_num[>]" => $order_num,
            ]
        );

        @unlink(self::Path . $id . $data['avatar']);
        
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function get($id) {
        $data = $this->db->select(
            self::Table , 
            '*', 
            [
                "id" => $id
            ]
        );
        // var_dump($data[0]['locale'][LibrarieLocale::EN]);
        return $data[0];
    }

    public function update($data, $uploadedFile) {

        // if($uploadedFile->getError() === UPLOAD_ERR_NO_FILE)


        $o_data = $this->db->select(
            self::Table , 
            '*', 
            [
                "id" => $data['id']
            ]
        );
        $o_data = $o_data[0];
        
        $this->db->action( function($db) use ($o_data, $data, $uploadedFile) {
            $updateColumns = [
                'name' => $data['name'],
                'tel' => $data['tel'],
                'update_datetime' => date('Y-m-d H:i:s')
            ];
            
            if($uploadedFile->getError() !== UPLOAD_ERR_NO_FILE) {
                $ext = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
                $updateColumns['avatar'] = $ext;
                $filename = $this->moveUploadedFile(self::Path, $uploadedFile, $o_data['id']);
            }

            $res = $db->update(
                self::Table, 
                $updateColumns,
                [
                    'id' => $o_data['id'],
                ]
        
            );            
            
            $error = $this->db->error();
            // var_dump($error);
            if( $error[2] != NULL ) {
                $this->log->write(json_encode($error));
                return false;
            }
            
        });

        return true;
        
    }

    /**
     * 搬移檔案
     *
     * @param [type] $directory
     * @param UploadedFile $uploadedFile
     * @param [string] $filename
     * @return void
     */
    private function moveUploadedFile($directory, UploadedFile $uploadedFile, $basename = '') {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        if($basename == '') {
            $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        }
        $filename = sprintf('%s.%0.8s', $basename, $extension);
    
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    
        return $filename;
    }
}

