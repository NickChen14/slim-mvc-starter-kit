<?php

namespace AdminApp\Model;
use Medoo\Medoo;
use AdminApp\Librarie\Log;

/** 
* Model Base
*  
* @category Model
* @package  Base
**/
class Base
{
    protected $log;
    protected $db;

    /**
     * 建構子
     */
    function __construct()
    {
        $this->log = new Log();
        $this->db = new Medoo(
            [
            'database_type' => 'mysql',
            'database_name' => getenv('DB_DATANAME'),
                   'server' => getenv('DB_SERVERER'),
                 'username' => getenv('DB_USERNAME'),
                 'password' => getenv('DB_PASSWORD'),
                  'charset' => 'utf8'
            ]
        );
    }
}

