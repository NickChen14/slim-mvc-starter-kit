<?php

namespace AdminApp\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;
use AdminApp\Librarie\Locale as LibrarieLocale;
use AdminApp\Model\Sample as ModelSample;

/** 
* Sample
*  
* @category Controller
* @package  Sample
**/
class Sample extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 列表
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function list($request, $response, $args)
    {   
        
        // $this->app->log->write('Sample');
        $uri = $request->getUri();
        $query = $uri->getQuery();
        $query = $request->getQueryParams($query);
        $ModelSample = new ModelSample();
        $res = $ModelSample->list($query['page']);
        $this->data['data'] = $res['data'];
        $this->data['currentPage'] = $res['currentPage'];
        $this->data['totalCount'] = $res['totalCount'];
        $this->data['totalPages'] = $res['totalPages'];
        // var_dump($this->data['data']);

        return $this->app->view->render('sample/list', ['data' => $this->data]);
    }

    /**
     * 新增
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function create($request, $response, $args)
    {   
        // $LibrarieLocale = new LibrarieLocale();
        $this->data['locale'] = LibrarieLocale::$Langs;
        
        if($request->isGet()) {
            return $this->app->view->render('sample/create', ['data' => $this->data]);
        }
        else if($request->isPost()) {
            $parsedBody = $request->getParsedBody();
            $uploadedFiles = $request->getUploadedFiles();
            $uploadedFile = $uploadedFiles['avatar'];
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                // $this->app->log->write(ModelSample::Path);
                $ModelSample = new ModelSample();
                $res = $ModelSample->create($parsedBody, $uploadedFile);
            }
            else {
                $this->error[] = 'Can not move file.';
                return $this->app->view->render('sample/create', ['data' => $this->data, 'error' => $this->error]);
            }

            

            // $this->app->log->write('-----------controller-----------');
            // $this->app->log->write($parsedBody);
            // $this->app->log->write('post:' . $res);
            
            // $this->data['args'] = $args;
            // var_dump($res);

            if($res == false) {
                $this->error[] = 'Data insert error.';
                return $this->app->view->render('sample/create', ['data' => $this->data, 'error' => $this->error]);
            }
            else {
                // return $this->app->view->render('sample/', ['data' => $this->data]);
                return $response->withRedirect('/admin/sample/');
            }
        }
        

        // $this->app->log->write('Sample');
        return $this->app->view->render('sample/create', ['data' => $this->data]);
    }

    /**
     * 排序
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function sort($request, $response, $args)
    {
        $uri = $request->getUri();
        $query = $uri->getQuery();
        $query = $request->getQueryParams($query);
        // var_dump($query);
        // var_dump($args);
        $ModelSample = new ModelSample();
        $res = $ModelSample->sort($args['id'], $query['type']);

        return $response->withRedirect('/admin/sample/?page=' . $query['page']);
    }

    /**
     * 刪除
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function delete($request, $response, $args)
    {
        $uri = $request->getUri();
        $query = $uri->getQuery();
        $query = $request->getQueryParams($query);

        $ModelSample = new ModelSample();
        $res = $ModelSample->delete($args['id']);
        return $response->withRedirect('/admin/sample/?page=' . $query['page']);
    }

    /**
     * 修改
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function update($request, $response, $args)
    {
        $this->data['locale'] = LibrarieLocale::$Langs;
        if($request->isGet()) {
            $ModelSample = new ModelSample();
            $data = $ModelSample->get($args['id']);
            $this->data['data'] = $data;
            return $this->app->view->render('sample/create', ['data' => $this->data]);
        }
        else if($request->isPost()) {
            
            $parsedBody = $request->getParsedBody();
            $uploadedFiles = $request->getUploadedFiles();
            $uploadedFile = $uploadedFiles['avatar'];
            
            $this->data['locale'] = LibrarieLocale::$Langs;
            
            if ($uploadedFile->getError() === UPLOAD_ERR_OK || $uploadedFile->getError() === UPLOAD_ERR_NO_FILE) {
                // $this->app->log->write(ModelSample::Path);
                $ModelSample = new ModelSample();
                $parsedBody['id'] = $args['id'];
                $res = $ModelSample->update($parsedBody, $uploadedFile);
            }
            else {
                $this->error[] = 'Can not move file.';
                return $this->app->view->render('sample/create', ['data' => $this->data, 'error' => $this->error]);
            }

            

            // $this->app->log->write('-----------controller-----------');
            // $this->app->log->write($parsedBody);
            // $this->app->log->write('post:' . $res);
            
            // $this->data['args'] = $args;
            // var_dump($res);

            if($res == false) {
                $this->error[] = 'Data insert error.';
                return $this->app->view->render('sample/create', ['data' => $this->data, 'error' => $this->error]);
            }
            else {
                // return $this->app->view->render('sample/', ['data' => $this->data]);
                return $response->withRedirect('/admin/sample/update/' . $args['id']);
            }
        }

        
        
        
        
        // var_dump($data);

        // $this->app->log->write('Sample');
        return $this->app->view->render('sample/create', ['data' => $this->data]);
    }
}


