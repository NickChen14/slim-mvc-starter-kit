<?php

namespace AdminApp\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* Account
*  
* @category Controller
* @package  Account
**/

class Account extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 首頁
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function index($request, $response, $args)
    {   
        require_once PWDPATH;

        
        $this->data['data'] = [ 'o_account' => $user, 'o_password' => $pwd ];

        return $this->app->view->render('account/add', ['data' => $this->data]);
    }

    /**
     * 修改帳號
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function post($request, $response, $args)
    {   
        $parsedBody = $request->getParsedBody();

        if($parsedBody['account'] != '' && $parsedBody['password'] != '' && $parsedBody['o_account'] != '' && $parsedBody['o_password'] != '') {
            require PWDPATH;
            if($parsedBody['o_account']==$user && $parsedBody['o_password']==$pwd) {
                
                $fp = fopen(PWDPATH, 'w');
                fwrite($fp, '<?php $user=\''.$parsedBody['account'].'\'; $pwd=\''.$parsedBody['password'].'\';?>');
                fclose($fp);

                require PWDPATH;
                $this->data['data'] = [ 'o_account' => $user, 'o_password' => $pwd ];
            }
            else {
                $this->error[] = 'Account, Password is invalid.';
            }
        }
        else {
            $this->error[] = 'Account, Password is required.';
        }
        
        

        return $this->app->view->render('account/add', ['data' => $this->data, 'error' => $this->error]);
    }

}


