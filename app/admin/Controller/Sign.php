<?php

namespace AdminApp\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* 登入
*  
* @category Controller
* @package  Sign
**/

class Sign extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 登入
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function index($request, $response, $args)
    {   

        // $this->app->log->write('Sign');
        
        // $response->getBody()->write(print_r($request->isDelete()));
        unset($_SESSION['USER']);
        
        $this->data['args'] = $args;



        return $this->app->view->render('signin', ['data' => $this->data]);
    }

    /**
     * 登入送出
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function post($request, $response, $args)
    {   
        $parsedBody = $request->getParsedBody();
        $ipAddress = $request->getAttribute('ip_address');

        if(!empty($parsedBody['g-recaptcha-response'])){
            if(ini_get('allow_url_fopen')) {
                $recaptcha = new \ReCaptcha\ReCaptcha(getenv('RECAPTCHA_SECRET'));
            }
            else {
                $recaptcha = new \ReCaptcha\ReCaptcha(getenv('RECAPTCHA_SECRET'), new \ReCaptcha\RequestMethod\SocketPost());
            }
            $resRecaptcha = $recaptcha->verify($parsedBody["g-recaptcha-response"], $ipAddress);
            if ($resRecaptcha->isSuccess()==false) {
                // echo "<p>You are a bot! Go away!</p>";
                $errorCodes = $resRecaptcha->getErrorCodes();
                foreach($errorCodes as $key => $value) {
                    // $captchamsg[] = $value;
                }
                $this->error[] = 'reCAPTCHA is invalid.';
                $this->app->log->write('Sign In error: ' . json_encode($this->error), 2);
                $this->app->log->write(json_encode($errorCodes), 2);
                $this->app->log->write('ip: ' . $ipAddress, 2);
        
            } 
            else if ($resRecaptcha->isSuccess()==true) {
                // echo "<p>You are not not a bot!</p>";
                if($parsedBody['account'] != '' && $parsedBody['password'] != '') {
                    require_once PWDPATH;
                    if($parsedBody['account']==$user && $parsedBody['password']==$pwd) {
                        $_SESSION['USER'] = true;
                        $_SESSION['ck_baseUrl'] = CKPATH;
                        return $response->withRedirect('/admin/');
                    }
                    else {
                        $this->error[] = 'Account, Password is invalid.';
                        $this->app->log->write('Sign In error: ' . json_encode($this->error), 2);
                        $this->app->log->write('ip: ' . $ipAddress, 2);
                    }
                }
                else {
                    $this->error[] = 'Account, Password is required.';
                    $this->app->log->write('Sign In error: ' . json_encode($this->error), 2);
                    $this->app->log->write('ip: ' . $ipAddress, 2);
                }
            }
        }
        else{
            $this->error[] = 'reCAPTCHA is required.';
            $this->app->log->write('Sign In error: ' . json_encode($this->error), 2);
            $this->app->log->write('ip: ' . $ipAddress, 2);
        }

        

        // $response->getBody()->write(print_r($_SESSION['USER']));
        // $response->getBody()->write($user);
        
        return $this->app->view->render('signin', ['data' => $this->data, 'error' => $this->error]);
    }

    /**
     * 登出
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function signout($request, $response, $args)
    {   
        $ipAddress = $request->getAttribute('ip_address');
        unset($_SESSION['USER']);
        $this->app->log->write('Sign out.');
        $this->app->log->write('ip: ' . $ipAddress);
        return $response->withRedirect('/admin/signin/');
    }
}


