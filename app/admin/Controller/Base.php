<?php

namespace AdminApp\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* Controller Base
*  
* @category Controller
* @package  Base
**/
class Base
{
    protected $app;
    protected $data;

    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        $this->app = $app;
    }
}

