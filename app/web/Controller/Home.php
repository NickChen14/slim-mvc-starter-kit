<?php

namespace App\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* 首頁
*  
* @category Controller
* @package  Home
**/
class Home extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 首頁
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function index($request, $response, $args)
    {   
        
        $this->data['args'] = $args;
        // $ipAddress = $request->getAttribute('ip_address');
        // $this->app->log->write('ip: ' . $ipAddress);

        return $this->app->view->render('home', ['data' => $this->data]);
    }
}


