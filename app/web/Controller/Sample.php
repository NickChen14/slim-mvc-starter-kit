<?php

namespace App\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;
use AdminApp\Model\Sample as ModelSample;

/** 
* Sample
*  
* @category Controller
* @package  Sample
**/
class Sample extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 首頁
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function list($request, $response, $args)
    {   
        



        $this->data['args'] = $args;
        // var_dump($args);
        $uri = $request->getUri();
        $query = $uri->getQuery();
        $query = $request->getQueryParams($query);
        $ModelSample = new ModelSample();
        $res = $ModelSample->list($query['page']);
        $this->data['data'] = $res['data'];
        $this->data['currentPage'] = $res['currentPage'];
        $this->data['totalCount'] = $res['totalCount'];
        $this->data['totalPages'] = $res['totalPages'];
        

        return $this->app->view->render('sample/list', ['data' => $this->data]);
    }

    /**
     * 詳細頁
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function detail($request, $response, $args)
    {   
        $this->data['args'] = $args;
        
        $ModelSample = new ModelSample();
        $res = $ModelSample->get($args['id']);
        $this->data['data'] = $res;

        
        // Title, Description, ....
        $this->data['title']            = $res['name'] . ' - Sample';


        return $this->app->view->render('sample/detail', ['data' => $this->data]);
    }
}


