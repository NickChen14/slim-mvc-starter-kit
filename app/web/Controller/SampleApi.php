<?php

namespace App\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;
use PHPMailer\PHPMailer\PHPMailer;

/** 
* Contact
*  
* @category Controller
* @package  Contact
**/
class Contact extends Base
{
    /**
    * 建構子
    *
    * @param interface $app 框架
    *
    * @return void
    **/
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }

    public function index($request, $response, $args)
    {   

        $parsedBody = $request->getParsedBody();
        $ipAddress = $request->getAttribute('ip_address');
        $res['ip'] = $ipAddress;

        if(!empty($parsedBody['g-recaptcha-response'])){
            if(ini_get('allow_url_fopen')) {
                $recaptcha = new \ReCaptcha\ReCaptcha(getenv('RECAPTCHA_SECRET'));
            }
            else {
                $recaptcha = new \ReCaptcha\ReCaptcha(getenv('RECAPTCHA_SECRET'), new \ReCaptcha\RequestMethod\SocketPost());
            }
            $resRecaptcha = $recaptcha->verify($parsedBody["g-recaptcha-response"], $ipAddress);
            if ($resRecaptcha->isSuccess()==false) {
                // echo "<p>You are a bot! Go away!</p>";
                $errorCodes = $resRecaptcha->getErrorCodes();
                foreach($errorCodes as $key => $value) {
                    // $captchamsg[] = $value;
                }

                $res['msg'] = 'reCAPTCHA is invalid.';
                $this->app->log->write('Mailer error: ' . json_encode($res), 2);
                $this->app->log->write(json_encode($errorCodes), 2);
        
            } 
            else if ($resRecaptcha->isSuccess()==true) {
                // echo "<p>You are not not a bot!</p>";
                if($parsedBody['name'] != '' && $parsedBody['email'] != '') {
                    
                    if($parsedBody['name'] != '' && $parsedBody['email'] != '') {
                        $mail = new PHPMailer();
                        $mail->CharSet = 'utf-8';
                        $mail->isSMTP();
                        //Enable SMTP debugging
                        // 0 = off (for production use)
                        // 1 = client messages
                        // 2 = client and server messages
                        $mail->SMTPDebug = 0;


                        //Set local server
                        /*
                        $mail->Host = 'localhost';  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = false;
                        $mail->Port = 25; 
                        */
                        
                        //Set the hostname of the mail server
                        $mail->Host = getenv('MAIL_SENDHOST');
                        // use
                        // $mail->Host = gethostbyname('smtp.gmail.com');
                        // if your network does not support SMTP over IPv6
                        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
                        $mail->Port = getenv('MAIL_SENDPORT');
                        //Set the encryption system to use - ssl (deprecated) or tls
                        $smtpSecure = getenv('MAIL_SENDSMTPSECURE');

                        if($smtpSecure == '') {
                            //Whether to use SMTP authentication
                            $mail->SMTPSecure = false;
                            $mail->SMTPAuth = false;
                        }
                        else {
                            //Whether to use SMTP authentication
                            $mail->SMTPSecure = 'tls';
                            $mail->SMTPAuth = true;
                        }
                        
                        //Username to use for SMTP authentication - use full email address for gmail
                        $mail->Username = getenv("MAIL_USERNAME");
                        //Password to use for SMTP authentication
                        $mail->Password = getenv("MAIL_USERPWD");
                        //Set who the message is to be sent from
                        $mail->setFrom(getenv("MAIL_SENDADDR"), getenv("MAIL_SENDNAME"));
                        //Set who the message is to be sent to
                        $mail->addAddress(getenv("MAIL_RECEIVEADDR"), getenv("MAIL_RECEIVENAME"));
                        //Set an alternative reply-to address
                        // $mail->addReplyTo('replyto@example.com', 'First Last');
                        $mail->Subject  = 'Website contact notification';
                        $mailBody = file_get_contents( APPSPATH . 'View/contact/mail.html');
                        $parsedBody['ip'] = $ipAddress;
                        $parsedBody['time'] = date('Y-m-d H:i:s');
                        foreach($parsedBody as $k => $v) {
                            $mailBody = str_replace("{{".$k."}}", $v, $mailBody);
                        }
                        $mail->msgHTML($mailBody, dirname(__FILE__));
                        // $mail->AltBody = 'This is a plain-text message body';
                        if(!$mail->send()) {
                            $res['code'] = '0001';
                            $res['msg'] = 'Mail send error';
                            // $this->app->log->write(APPSPATH . 'view/contact/mail.html');
                            $this->app->log->write('Mailer error: ' . $mail->ErrorInfo, 2);
                        } else {
                            $res['code'] = '0000';
                            $res['msg'] = 'Mail send success.';
                            $this->app->log->write('Mailer success: ' . json_encode($res));
                        }
                    }
                    else {
                        $res['code'] = '0001';
                        $res['msg'] = 'Name, Email is invalid.';
                        $this->app->log->write('Mailer error: ' . json_encode($res), 2);
                    }
                }
                else {
                    $res['code'] = '0001';
                    $res['msg'] = 'Name, Email is required.';
                    $this->app->log->write('Mailer error: ' . json_encode($res), 2);
                }
            }
        }
        else{
            $res['msg'] = 'reCAPTCHA is required.';
            $this->app->log->write('Mailer error: ' . json_encode($res), 2);
        }
        
        // $this->app->log->write($parsedBody);
        $response->withHeader('Content-type', 'application/json');
        $response->getBody()->write(json_encode($res));
        // $response->withJson($res);
        // return $this->app->render(200, $res);
    }
}


