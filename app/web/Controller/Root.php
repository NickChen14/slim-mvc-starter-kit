<?php

namespace App\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* 根目錄
*  
* @category Controller
* @package  Root
**/

class Root extends Base
{
    /**
     * 建構子
     *
     * @param ContainerInterface $app
     */
    function __construct(ContainerInterface $app)
    {
        parent::__construct($app);
    }
    
    /**
     * 根目錄導向預設語系的首頁
     *
     * @param [type] $request
     * @param [type] $response
     * @param [type] $args
     * @return void
     */
    public function index($request, $response, $args)
    {   
        return $response->withStatus(302)->withHeader('Location', '/en/home');
    }
}


