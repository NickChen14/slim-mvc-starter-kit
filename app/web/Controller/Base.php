<?php

namespace App\Controller;
use Psr\Container\ContainerInterface as ContainerInterface;

/** 
* Controller Base
*  
* @category Controller
* @package  Base
**/
class Base
{
    protected $app;
    protected $data;

    /**
    * 建構子
    *
    * @param interface $ci 框架
    *
    * @return void
    **/
    function __construct(ContainerInterface $app)
    {
        $this->app = $app;
    }
}

