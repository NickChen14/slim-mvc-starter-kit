<?php $this->layout('Layout::default', ['data' => $data]); ?>
<hr class="bt-hr bt-hr--transparent-50 hidden-md hidden-lg">
<hr class="bt-hr bt-hr--transparent-100 hidden-xs hidden-sm">
<h1 class="text-center bt-heading bt-heading--h1">404</h1>
<hr class="bt-hr bt-hr--transparent-10">
<p class="text-center bt-heading bt-heading--h2">Page Not Found.</p>
<hr class="bt-hr bt-hr--transparent-10">
<p class="text-center">Back to <a href="<?=$baseLocaleUrl?>home">Home</a></p>
<hr class="bt-hr bt-hr--transparent-50 hidden-md hidden-lg">
<hr class="bt-hr bt-hr--transparent-100 hidden-xs hidden-sm">
