<?php $this->layout('Layout::default', ['data' => $data]); ?>
<section class="sample-page__home text-center">
    <br><br><br><br><br><br><br>
    <h1>Sample</h1>
    <br><br>
    <table class="table text-center">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">Avatar</th>
                <th class="text-center">Name</th>
                <th class="text-center">Tel</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['data'] as $k => $array):?>
            <tr>
                <td><span class="text-muted"><?=$array['id']?></span></td>
                <td>
                    <span class="img-circle" style="background: url(<?=$array['avatar']?>) no-repeat 50% 50%; background-size: cover; width: 50px; height: 50px; display: inline-block;"></span>
                </td>
                <td><a href="<?=$baseLocaleUrl?>sample/<?=$array['id']?>"><?=$this->e($array['name'])?></a></td>
                <td><?=$this->e($array['tel'])?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>


    <a href="<?=$baseLocaleUrl?>home">Go Home</a>
    <br><br><br><br><br><br><br>
</section>
