<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="<?=$locale['html']?>" class="no-js ie6 oldie" ng-app="myApp"> <![endif]-->
<!--[if IE 7]>    <html lang="<?=$locale['html']?>" class="no-js ie7 oldie" ng-app="myApp"> <![endif]-->
<!--[if IE 8]>    <html lang="<?=$locale['html']?>" class="no-js ie8 oldie" ng-app="myApp"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="<?=$locale['html']?>" class="no-js" ng-app="myApp"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <title><?=($this->e($data['title'])) ? $this->e($data['title']) : $this->e($title); ?></title>
        <meta name="keywords" content="<?=($this->e($data['keywords'])) ? $this->e($data['keywords']) : $this->e($keywords); ?>">
        <meta name="description" content="<?=($this->e($data['description'])) ? $this->e($data['description']) : $this->e($description); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?=$baseUrl?>images/favico/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>images/favico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>images/favico/favicon-16x16.png">
        <link rel="manifest" href="<?=$baseUrl?>images/favico/site.webmanifest">
        <link rel="mask-icon" href="<?=$baseUrl?>images/favico/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?=$baseUrl?>images/favico/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?=$baseUrl?>images/favico/mstile-144x144.png">
        <meta name="msapplication-config" content="<?=$baseUrl?>images/favico/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <!-- Open Graph data-->
        <meta property="og:title" content="<?=($this->e($data['title'])) ? $this->e($data['title']) : $this->e($title); ?>">
        <meta property="og:description" content="<?=($this->e($data['description'])) ? $this->e($data['description']) : $this->e($description); ?>">
        <meta property="og:image" content="<?=($this->e($data['image'])) ? $this->e($data['image']) : $this->e($image); ?>">
        <link rel="image_src" href="">
        <!-- Schema.org markup for Google+-->
        <meta itemprop="name" content="<?=($this->e($data['title'])) ? $this->e($data['title']) : $this->e($title); ?>">
        <meta itemprop="image" content="<?=($this->e($data['image'])) ? $this->e($data['image']) : $this->e($image); ?>">
        <meta itemprop="description" content="<?=($this->e($data['description'])) ? $this->e($data['description']) : $this->e($description); ?>">
        <!-- Twitter Card data-->
        <meta name="twitter:title" content="<?=($this->e($data['title'])) ? $this->e($data['title']) : $this->e($title); ?>">
        <meta name="twitter:description" content="<?=($this->e($data['description'])) ? $this->e($data['description']) : $this->e($description); ?>">
        <meta name="twitter:card" content="<?=($this->e($data['image'])) ? $this->e($data['image']) : $this->e($image); ?>">
        <!-- css-->
        <link rel="stylesheet" href="<?=$baseUrl?>css/plugin.css">
        <link rel="stylesheet" href="<?=$baseUrl?>css/screen.css">
        <script src="<?=$baseUrl?>js/config.js"></script>
        <script src="<?=$baseUrl?>js/plugin.min.js"></script>
        <script src="<?=$baseUrl?>js/app.min.js"></script>


		<?php if(isset($data['css'])): ?>
			<?php foreach ($data['css'] as $c): ?>
            <link href="<?=$this->e($c); ?>" rel="stylesheet">
			<?php endforeach; ?>
        <?php endif; ?>
        <?php if(isset($data['script'])): ?>
			<?php foreach ($data['script'] as $s): ?>
            <script src="<?=$this->e($s); ?>"></script>
			<?php endforeach; ?>
		<?php endif; ?>
    </head>
    <body>
        
    
        <?php $this->insert('partials/nav') ?>
        

        <main class="sample-main">
            <?=$this->section('content'); ?>
            
        </main>
    
        <?php $this->insert('partials/footer') ?>
    </body>
</html>
