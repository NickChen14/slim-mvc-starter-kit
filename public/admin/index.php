<?php

define('VERSIONR', '1.0.0');
define('STAGING' , 'admin');
define('BASEPATH', __DIR__);
define('ROOTPATH', BASEPATH . DIRECTORY_SEPARATOR . '../..'   . DIRECTORY_SEPARATOR);
define('COREPATH', ROOTPATH . 'core' . DIRECTORY_SEPARATOR . STAGING . DIRECTORY_SEPARATOR);
define('APPSPATH', ROOTPATH . 'app'  . DIRECTORY_SEPARATOR . STAGING . DIRECTORY_SEPARATOR);
define('LOGSPATH', ROOTPATH . 'log'  . DIRECTORY_SEPARATOR . STAGING . DIRECTORY_SEPARATOR);
define('PWDPATH' , ROOTPATH . 'public' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'password.php');
define('CKPATH'  , DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'ck' . DIRECTORY_SEPARATOR);

// echo LOGSPATH;
// echo COREPATH;
// exit;

require ROOTPATH . 'vendor/autoload.php';

$env = new Dotenv\Dotenv(ROOTPATH);
$env->load();

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$set = include COREPATH . 'config.php';
$app = new \Slim\App($set);

require COREPATH . 'dependencies.php';
require COREPATH . 'middleware.php';
require COREPATH . 'routes.php';

$app->run();
