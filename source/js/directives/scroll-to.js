myApp.directive("scrollTo", 
	['$rootScope', '$log',
	function($rootScope, $log) {
		return {
			restrict: 'A',
			scope: {
				target: "=",
			},

			link: function(scope, element, attrs, controllers) {
                var el = element[0];
                $(el).click(function(){
                    $log.debug($rootScope.NavToggle);
                    $('html, body').stop().animate({
                        scrollTop: $(scope.target).offset().top
                    }, 500);

                    $rootScope.NavToggle = false;
                    scope.$apply();
                })
                
			}
		};
	}
]);
