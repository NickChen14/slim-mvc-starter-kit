/*
 * Angular Factory
 * Name: String
 * Description: 共用正規表示法
 * Author: Nick
 * Modify Date: 2018-05-21 19:20
 */
myApp.factory('REGEX',
	['$log',
	function ($log) {

	$log.debug('----載入共用正規表示法----');
	// $log.debug('EX:');
	// $log.debug('$scope.REGEX = REGEX;');
	// $log.debug('ng-pattern="REGEX.cellphone.pattern" maxlength="{{ REGEX.cellphone.maxlength }}" placeholder="{{ REGEX.cellphone.placeholder }}"');
	// $log.debug('----------------------------');

	// 增加擴充性
	return {
        vipcode: {
            pattern: /^[0-9]{3,3}$/,
			maxlength: 3,
			placeholder: '請輸入您邀請函上 3 碼首選貴賓編號',
			required: '請輸入您邀請函上 3 碼首選貴賓編號',
			invalid: '請輸入正確貴賓編號'
        },
        cellphonefinal6: {
            pattern: /^[0-9]{6,6}$/,
			maxlength: 6,
			placeholder: '請輸入您行動電話後 6 碼',
			required: '請輸入您行動電話後 6 碼',
			invalid: '請輸入正確行動電話後 6 碼'
		},
		plusone: {
			required: '請選擇是否攜伴',
		},
		cellphone: {
			pattern: /^09[0-9]{8}$/,
			maxlength: 10,
			placeholder: '您的手機號碼',
			required: '請輸入您的手機號碼',
			invalid: '請輸入正確手機號碼'
		},
		price: {
			pattern: /^[1-9][0-9]{0,3}$|[0-9]{0,4}(\.{1}[0-9]{1})$/,
			maxlength: 6,
		},
		message: {
			pattern: /^[0-9]{4}$/,
			maxlength: 4,
			placeholder: '手機簡訊4位數驗證碼',
		},
		captcha: {
			pattern: /^[0-9a-zA-Z]{5}$/,
			maxlength: 5,
			placeholder: '請輸入驗證碼',
		},
		password: {
			pattern: /^[a-zA-Z0-9]{6,12}$/,
			maxlength: 12,
			placeholder: '請設定6-12位英數字密碼',
		},
		email: {
			pattern: /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/,
			maxlength: 100,
			placeholder: '請輸入您的聯繫Email',
		},
		area: {
			pattern: /^[0-9]{1,4}$/,
			maxlength: 4,
			placeholder: '區碼',
		},
		tel: {
			pattern: /^[1-9][0-9]{6,12}$/,
			maxlength: 13,
			placeholder: '市話號碼',
		},
		ext: {
			pattern: /^[0-9]{0,6}$/,
			maxlength: 6,
			placeholder: '分機',
		},
		vat: {
			pattern: /^[0-9]{8}$/,
			maxlength: 8,
			placeholder: '公司統一編號',
		},
		address: {
			maxlength: 120,
			placeholder: '地址',
			placeholderOption: '地址(選填)',
		},
		lineID: {
			maxlength: 20,
		},
		name: {
			maxlength: 20,
			placeholder: '您的姓名或暱稱',
			required: '請輸入您的姓名或暱稱',

		},
		guildNo: {
			pattern: /^[a-zA-Z0-9]{0,30}$/,
			maxlength: 30,
		},
		img: {
			pattern: /\.(jpe?g|png|gif)$/i,
			accept: 'image/png,image/jpg,image/jpeg,image/gif',
			// maxsize: 5 * 1024
			maxsize: 5000000
		},
		pdf: {
			pattern: /\.(jpe?g|png|gif|pdf)$/i,
			accept: 'image/png,image/jpg,image/jpeg,image/gif,application/pdf',
			maxsize: 5000000
		},
		desc: {
			maxlength: 2000,
		},

	};
}]);
