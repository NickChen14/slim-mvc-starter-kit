/*
 * Angular Factory
 * Name: COPY
 * Description: 共用文案，可使用在無關SEO的文案上
 * Author: Nick
 * Modify Date: 2018-03-22 10:28
 */
myApp.factory('COPY',
	['$log',
	function ($log) {
	$log.debug('----載入共用文案，可使用在無關SEO的文案上----');
	
    return {
		// 檔案（圖片）
		fileupload: '支援檔案類型： gif、jpg、jpeg、png檔，單一圖檔尺寸上限：5mb',
		maximg: '上傳圖片超過上限',
		maxsize: '檔案大小超過 5 mb 上限，請重新上傳',
		invalidimg: '僅支援上傳 gif、jpg、jpeg、png 格式，請重新上傳',
		invalidpdf: '僅支援上傳 pdf、gif、jpg、jpeg、png 格式，請重新上傳',
		invalidimgurl: '圖片網址不支援非 http 網址',

		// 結果
		searchresult: '查詢結果',

		// 表單資料驗證
		wrongpassword: '密碼不正確',
		wrongphone: '此手機號碼已註冊',
		wrongcaptcha: '驗證碼不正確',
		wrongemail: '此 email 已註冊',
		wrongconfirmation: '兩組新密碼內容不同',

		
		// 表單格式驗證
		required: '此為必填欄位',
		maxstring: '字數超過上限',
		maxnumstring: function (int) {
			return '字數超過 '+int+' 字上限';
		},

		invalidemail: '請填寫正確電子信箱',
		invalidphone: '請輸入正確手機號碼',
		invalidtel: '電話格式不正確',
		invalidint: '請填入半形整數數字',
		invalidnumber: '請輸入半形數字',
		invalidfloat: '請輸入小數點1位以內半形數字',
		invalidvatno: '請輸入正確統編號碼',
		invalidpassword: '請輸入 6-12 位半形英數字',
		invalidcaptcha: '驗證碼格式不正確',
		
		// 其他
		requiredfile: '請選擇上傳檔案',
		requiredlessfour: '請輸入 4 位數以內數字',
		requiredonepoint: '請輸入小數點1位以內半形數字',

    };
}]);
