myApp.constant({
    AppConfig: {
        ScreenSmall: 320,
        ScreenMedium: 767,
        ScreenLarge: 1023,
        Tel: Tel,
        Phone: Phone,
        Mail: Mail,
        Api: Api,
        Root: Root,

    },
});
