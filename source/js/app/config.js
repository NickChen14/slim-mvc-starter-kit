myApp.config([
    '$locationProvider',
    '$logProvider',
    'AppConfig',
    '$httpProvider',
    function($locationProvider, $logProvider, AppConfig, $httpProvider) {
        // Log debug setting
        $logProvider.debugEnabled(!isProduction);
        // html5Mode
        $locationProvider.html5Mode(false);

    }
]);
