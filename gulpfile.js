var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    gulpIf = require('gulp-if'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    iconfont = require('gulp-iconfont'),
    connect = require('gulp-connect-php'),
    env = require('gulp-env'),
    iconfont = require('gulp-iconfont'),
    consolidate = require("gulp-consolidate");
    spritesmith = require('gulp.spritesmith');
    header = require('gulp-header'),
    merge = require('merge-stream'),
    modernizr = require('gulp-modernizr'),
    imagemin = require('gulp-imagemin'),
    imageminMozjpeg = require('imagemin-mozjpeg'),
    imageminPngquant = require('imagemin-pngquant'),
    realFavicon = require('gulp-real-favicon'),
    fs = require('fs');

var reload = browserSync.reload;
//environment variables
var env = JSON.parse(fs.readFileSync('.env.json'));

var config = '"use strict";\n';
config += 'var isProduction = <%= env.isProduction.toString() %>;\n';
var pkg = JSON.parse(fs.readFileSync('package.json')),
    banner = '/*! <%= new Date().toDateString() %> | <%= pkg.name %> | <%= pkg.description %> | <%= pkg.author %> */\n',
    src = 'source',
    dest = 'public',
    app = 'app',
    assets = 'assets';

gulp.task('php', function() {
    connect.server({
        base: dest,
        port: env.PHPPORT,
        // ini: '/usr/local/php5/lib'
    });
});

gulp.task('serve', ['php'], function() {
    browserSync.init({
        proxy: env.PROXY,
        port: env.PORT,
        notify: false,
        open: true,
        // logLevel: "debug"
    });

    gulp.watch(dest + '/images/icons/*.png', ['make:sprite']);
    gulp.watch(src + '/sass/**/*.scss', ['make:css']);
    gulp.watch(
        [src + '/js/app.js', src + '/js/controllers.js', src + '/js/directives/*.js', src + '/js/app/*.js', src + '/js/controllers/*.js'], ['make:app-js']
    );
    // gulp.watch(src + '/js/main.js', ['make:js']);


    gulp.watch(dest + '/js/*.js').on('change', reload);
    gulp.watch([
        app + '/**/*.php', 
        dest + '/*.php', 
        app + '/view/**/*.html',
        dest + '/admin/app/*.php',
        dest + '/admin/core/*.php',
        dest + '/admin/view/**/*.php',
        dest + '/admin/*.php'
    ]).on('change', reload);
});

gulp.task('make:css', function() {
    return gulp.src(src + '/sass/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.beep();
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))
        .pipe(sass())
        .pipe(autoprefixer(eval(env.autoprefixer)))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))
        .pipe(gulpIf(env.isProduction, cleanCSS({ compatibility: 'ie8' })))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/css')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/css')))
        .pipe(browserSync.stream());
});

gulp.task('make:plugin-css', function() {
    return gulp.src([
            src + '/bower_components/jquery-ui/themes/ui-lightness/jquery-ui.css',
            src + '/bower_components/animate.css/animate.min.css',
            src + '/bower_components/slick-carousel/slick/slick.css',
            src + '/bower_components/magnific-popup/dist/magnific-popup.css',
        ])
        .pipe(concat('plugin.css'))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))
        .pipe(gulpIf(env.isProduction, cleanCSS({ compatibility: 'ie8' })))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/css')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/css')))
        .pipe(browserSync.stream());
});

gulp.task('make:ie8-js', function() {
    return gulp.src([
            src + '/bower_components/respond/dest/respond.src.js',
            // src + '/js/plugin/html5shiv.js'

        ])
        .pipe(plumber())
        .pipe(concat('ie8-fix.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/js')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/js')))
});

gulp.task('make:plugin-js', function() {
    return gulp.src([
            src + '/bower_components/jquery/dist/jquery.js',
            src + '/bower_components/jquery-ui/jquery-ui.js',
            src + '/bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.js',
            src + '/bower_components/angular/angular.js',
            src + '/bower_components/angular-animate/angular-animate.js',
            src + '/bower_components/angular-sanitize/angular-sanitize.js',
            // src + '/bower_components/angular-ui-router/release/angular-ui-router.js',
            // src + '/bower_components/angular-translate/angular-translate.js',
            // src + '/bower_components/angular-breadcrumb/dist/angular-breadcrumb.js',
            // src + '/bower_components/angular-paging/dist/paging.js',
            // src + '/bower_components/angular-recaptcha/release/angular-recaptcha.js',
            src + '/bower_components/magnific-popup/dist/jquery.magnific-popup.js',
            src + '/plugin/modernizr/modernizr.js',
            src + '/bower_components/PACE/pace.js',
            // src + '/bower_components/lodash/dist/lodash.js',
            src + '/bower_components/slick-carousel/slick/slick.js',
            // src + '/bower_components/angular-slick-carousel/dist/angular-slick.js',
            // src + '/bower_components/ScrollToFixed/jquery-scrolltofixed.js',
            // src + '/bower_components/scrolltofixed-ng/src/scrolltofixed.js',
            // src + '/bower_components/alasql/dist/alasql.min.js',
            // src + '/bower_components/alasql/console/xlsx.core.min.js',


        ])
        .pipe(concat('plugin.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/js')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/js')));
});

gulp.task('make:app-js', function() {
    return gulp.src([
            src + '/js/app.js',
            src + '/js/app/*.js',
            src + '/js/controllers.js',
            src + '/js/factory/*.js',
            src + '/js/controllers/*.js',
            src + '/js/components/*.js',
            src + '/js/directives/*.js',
            src + '/js/model/*.js',
            src + '/js/filter/*.js',
        ])
        .pipe(concat('app.min.js'))
        .pipe(header(config, { env: env }))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/js')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/js')));
    // .pipe(browserSync.stream());
});

gulp.task('modernizr', function() {

	gulp.src([
		dest + '/js/*.js',
		dest + '/css/*.css',
	])
	  .pipe(modernizr({
		"options" : [
			"setClasses",
			"addTest",
			"html5printshiv",
			"testProp",
            "fnBind",
            "mq"
		],
		"excludeTests": ['hidden']
	}))
	  .pipe(gulp.dest(src + '/plugin/modernizr'))
  });


gulp.task('make:js', function() {
    return gulp.src([
            src + '/js/main.js'
        ])
        .pipe(concat('main.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulpIf(env.isProduction, gulp.dest(assets + '/js')))
        .pipe(gulpIf(!env.isProduction, gulp.dest(dest + '/js')));
})

gulp.task('make:iconfont', function() {
    var fontName = 'bd-icons';
    var template = 'icons-tpl';
    return gulp.src(['source/icons/*.svg'])
        .pipe(iconfont({
            fontName: fontName,
            normalize: true,
            fontHeight: 1001,
            formats: ['ttf', 'eot', 'woff']
        }))
        .on('glyphs', function(glyphs) {
            var options = {
                glyphs: glyphs.map(function(glyph) {
                    return {
                        name: glyph.name.substr(3),
                        codepoint: glyph.unicode[0].charCodeAt(0)
                    }
                }),
                fontName: fontName,
                fontPath: '../fonts/',
                className: 'bd-icons'
            };

            gulp.src(src + '/icons/template/' + template + '.css')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: fontName }))
                .pipe(gulp.dest(dest + '/css/'));

            gulp.src(src + '/icons/template/' + template + '.html')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: 'sample' }))
                .pipe(gulp.dest(dest + '/css/'));
        })
        .pipe(gulp.dest(dest + '/fonts/'));
});

gulp.task('make:sprite', function() {
    // console.log(dest + 'images/icons/*.png');
    var spriteData = gulp.src(dest + '/images/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprites-icons.scss',
        cssFormat: 'scss',
        imgPath: '../images/sprite.png'
    }));

    var imgStream = spriteData.img.pipe(gulp.dest(dest + '/images/'));
    var cssStream = spriteData.css.pipe(gulp.dest(src + '/sass/sprites/'));
    return merge(imgStream, cssStream);
    // return imgStream;
});

gulp.task('make:imagemin', function() {
    gulp.src(dest + '/images/**/*')
    .pipe(imagemin(
        [
        
            imagemin.gifsicle({interlaced: true}),
            imageminPngquant({
                quality: '80',
                speed: 1,
                floyd:0
              }),
            imageminMozjpeg({ quality: 80, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            
        ],
        {
            verbose: true,
        }
))
	.pipe(gulp.dest(assets + '/images'))
});


// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';
// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
    realFavicon.generateFavicon({
        masterPicture: 'favico/ico.png',
        dest: dest + '/images/favico/',
        iconsPath: 'images/favico/',
        design: {
            ios: {
                pictureAspect: 'noChange',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                }
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
				backgroundColor: '#ffffff',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                }
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: '#ffffff',
                manifest: {
                    display: 'standalone',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'silhouette',
                themeColor: '#5bbad5'
            }
        },
        settings: {
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function() {
        done();
    });
});

// Inject the favicon markups in your HTML pages. You should run
// this task whenever you modify a page. You can keep this task
// as is or refactor your existing HTML pipeline.
gulp.task('inject-favicon-markups', function() {
    return gulp.src(['favico/template.html'])
        .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
        .pipe(gulp.dest(dest + '/images/favico'));
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
    var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
    realFavicon.checkForUpdates(currentVersion, function(err) {
        if (err) {
            throw err;
        }
    });
});



gulp.task('build:env', function() {
    env.isProduction = true;
});

//run gulp
gulp.task('default', 
    [
        'make:sprite'
        , 'make:css'
        , 'make:plugin-css'
        , 'make:app-js'
        , 'make:js'
        , 'make:plugin-js'
        , 'make:ie8-js'
        // , 'make:iconfont'
        , 'serve'
    ]
);

gulp.task('build:plugin', 
    [
        'make:plugin-js'
        , 'make:ie8-js'
        , 'make:plugin-css'
    ]
);

gulp.task('build', 
    [
        'build:env'
        , 'make:sprite'
        , 'make:css'
        , 'make:plugin-css'
        , 'make:app-js'
        , 'make:js'
        , 'make:plugin-js'
        , 'make:ie8-js'
        // , 'make:iconfont'
    ]
);

gulp.task('build:check'
, ['serve']);
