# Slim-Skeleton-MVC

這是一個前後台的管理系統架構，可自行調整

- Slim + MVC + SPA
- Slim + MVC + SSR

這範例是用angularjs來配合SSR架構。

SQL: [Medoo](https://github.com/catfan/Medoo/)  
View Template: [Plates](https://github.com/thephpleague/plates)  
ENV: [PHP dotenv](https://github.com/vlucas/phpdotenv)  
Admin Panel: [Tabler](https://tabler.github.io/tabler/)  

## Install

使用 Composer 安裝

composer create
```
composer create-project nickchen14/slim-mvc-angularjs-starter-kit [your-app]
```

OR  

git download

```
git clone git@gitlab.com:NickChen14/slim-mvc-starter-kit.git
```

------

安裝composer套件

```
composer install
```

------

安裝npm套件

```
npm install
```

------
安裝bower套件

```
bower install
```

## Start

### 啟動WebServer

[Slim Documentation Web Servers](https://www.slimframework.com/docs/start/web-servers.html)

OR  

``` 
gulp
```

### Env

.env.example 改為 .env 並修改內容。

```
WEB_URL="http://localhost:3044/"                # 網站URL
DB_SERVERER="127.0.0.1"                         # 資料庫IP
DB_USERNAME="nick"                              # 資料庫登入USER
DB_PASSWORD="nick"                              # 資料庫登入密碼
DB_DATANAME="sample"                            # 資料庫名稱
MAIL_SENDHOST="smtp.gmail.com"
MAIL_SENDPORT="587"
MAIL_SENDSMTPSECURE='tls'
MAIL_USERNAME="xxxxxxxxxx@gmail.com"            # 發信用帳號
MAIL_USERPWD="xxxxxxxxx"                        # 發信用密碼
MAIL_SENDADDR="xxxxxxxxxxxxxxx@gmail.com"       # 寄信用帳號
MAIL_SENDNAME="Nick"                            # 寄信用名稱
MAIL_RECEIVEADDR="xxxxxxxxxxxxxxx@gmail.com"    # 收信用帳號
MAIL_RECEIVENAME="xxxxxxxxxxxxxxx@gmail.com"    # 收信用名鞥
RECAPTCHA_SECRET="xxxxxxxxxxxxxxx"              # GOOGLE RECAPTCHA
```
google recaptcha僅限本機使用請更換


### 新增URL

[Slim Documentation Router](https://www.slimframework.com/docs/objects/router.html)

core/web/routes.php  
core//admin/routes.php 
